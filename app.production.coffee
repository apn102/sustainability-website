axis         = require 'axis'
rupture      = require 'rupture'
jeet         = require 'jeet'
autoprefixer = require 'autoprefixer-stylus'
js_pipeline  = require 'js-pipeline'
css_pipeline = require 'css-pipeline'
dynamic_content = require 'dynamic-content'


module.exports =
  ignores: ['readme.md', '**/layout.*', '**/_*', '.gitignore', 'ship.*conf']

  extensions: [
    js_pipeline(files: 'assets/js/*.*',  out: 'js/build.js', minify: true),
    css_pipeline(files: 'assets/css/*.styl', out: 'css/build.css', minify: false)
    dynamic_content()
  ]

  stylus:
    use: [axis(), rupture(), jeet(), autoprefixer()]
