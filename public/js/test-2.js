// external js: isotope.pkgd.js


/// did I just miss data group fromt the divs?????


// init Isotope
var $grid = $('#events-container').isotope({
  itemSelector: '.events',
  layoutMode: 'fitRows'
});
// filter items on button click
$('#event-categories').on( 'click', 'a', function() {
  var filterValue = $(this).attr('data-filter');
  $('#event-categories a.checked').not(this).removeClass('checked');
  $(this).addClass('checked');
  $grid.isotope({ filter: filterValue });
});


function init() {
    window.addEventListener('scroll', function(e){
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 300,
            header = document.querySelector("header");
        if (distanceY > shrinkOn) {
            classie.add(header,"smaller");
        } else {
            if (classie.has(header,"smaller")) {
                classie.remove(header,"smaller");
            }
        }
    });
}
window.onload = init();
